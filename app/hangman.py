import random

# 45 letters is the longest word in the english language
# 0 - 3 letters easy 3 - 6 letters medium 6 - 45 hard,
difficulty_levels = [0, 3, 6, 45]
word_list = [
    "pad",
    "eye",
    "toe",
    "happy",
    "hello",
    "ground",
    "obscure",
    "complect",
    "contemplate",
    "homoiconicity",
]


def select_word(difficulty):
    """Given a difficulty level between 1 and 3 select a word from our word list"""
    if difficulty not in [1, 2, 3]:
        return None
    lower_bound = difficulty_levels[difficulty - 1]
    upper_bound = difficulty_levels[difficulty]
    words = [
        word
        for word in word_list
        if len(word) > lower_bound and len(word) <= upper_bound
    ]
    if words:
        return random.choice(words)


class Player:
    def __init__(self, name):
        self.name = name


class UiData:
    "Handle data input and output for the game which could be sent and received over an api."

    def __init__(self):
        self.msgs = []

    def msg(self, msg):
        self.msgs.append(msg)
        return self

    def user_input(self, msg, data=None):
        return data["input"]

    def render(self):
        json = self.msgs
        self.msgs = []
        return json


class Ui:
    "Handle console input to the game"

    def __init__(self):
        self.msgs = []

    def msg(self, msg):
        self.msgs.append(msg)
        return self

    def user_input(self, msg, data=None):
        return input(msg).lower()

    def render(self):
        for msg in self.msgs:
            print(msg)
        self.msgs = []


class Game:
    def __init__(self, ui=Ui):
        "We could pass in ui and override how we render, we could generate a json payload as an example"
        self.ui = ui()

    def setup(self, data=None):
        "Randomly select a word based on the difficulty Level selected"
        user_selection = self.ui.user_input(
            "Select difficulty level Easy, Medium, Hard: ", data
        )
        difficulty_level = {"easy": 1, "medium": 2, "hard": 3}.get(user_selection, 1)
        self.set_word(select_word(difficulty_level))

    def set_word(self, word):
        "Set the hidden word, can be passed a random word or a user controlled word"
        self.game_iterations = 0
        self.lives = 5
        self.guessed_letters = set()
        self.word = word

        # just for / testing at the command line
        # print(self.word)
        self.hidden_word = ["_"] * len(self.word)

    def start_loop(self, data=None):
        """Loop until one of the complete conditions is met ie no lives or word matched"""

        self.setup(data)
        while self.continue_game():
            self.ui.render()
            self.take_next_turn()
        self.ui.render()

    def take_next_turn(self, data=None):
        "Take another step in the game, taking the users input and handling it"
        guess = self.ui.user_input(": ", data)

        # handle single or multiple chars
        result = []
        for letter in guess:
            result.append(self.guess_letter(letter))

    def continue_game(self):
        "Check if any game end conditions have been met"
        if self.lives == 0:
            self.ui.msg("Game over no lives left")
            return False

        if "_" not in self.hidden_word:
            self.ui.msg(f"Guessed correctly game finished with {self.lives} lives left")
            return False

        self.ui.msg(
            f'Type your next guess {"".join(self.hidden_word)} you have {self.lives} lives left :'
        )

        return True

    def guess_letter(self, letter):
        "Handle letter guess, reduce lives if letter not in word"

        # Handle the situation when a user selects a letter
        if letter in self.guessed_letters:
            self.ui.msg(f"You already guessed the letter {letter}")
            return

        # Add the guessed letter to the set
        self.guessed_letters.add(letter)

        # Update the hidden word replacing the blank with the guessed letter
        # but only if the letter is in the generated word for this game
        if letter in self.word:
            for i, c in enumerate(self.word):
                if c == letter:
                    self.hidden_word[i] = letter
        else:
            self.ui.msg(f"Your guess of {letter} is not in the word you lost a life!")
            self.lives -= 1

    def play_again(self, data=None):
        "Ask the user if they would like to continue playing"
        play_again = self.ui.user_input("Play again Y/N ? ")
        if "n" == play_again:
            return False
        return True


# Game loop for console version
if __name__ == "__main__":
    "Console version using a loop to allow the player to keep playing."
    playing = True
    game = Game(Ui)

    while playing:
        game.start_loop()
        playing = game.play_again()
