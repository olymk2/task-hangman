import pytest
from mock import patch
from hangman import select_word, Game, Ui, UiData


@patch("hangman.input", side_effect=["easy", "es", "s", "q", "t"])
def test_console_game(input_mock):
    """Example flow of hangman application mocking out print and input, to aid in testing"""
    print_history = []

    def fake_print(msg):
        print_history.append(msg)

    with patch("hangman.print", wraps=fake_print) as mock_bar:
        game = Game(Ui)

        game.setup()
        game.set_word("test")

        # first letters
        game.continue_game()
        game.take_next_turn()
        game.ui.render()
        assert print_history == ["Type your next guess ____ you have 5 lives left :"]

        # test duplicate letters
        game.continue_game()
        game.take_next_turn()
        game.ui.render()
        assert print_history == [
            "Type your next guess ____ you have 5 lives left :",
            "Type your next guess _es_ you have 5 lives left :",
            "You already guessed the letter s",
        ]

        # test letter not in the answer
        game.continue_game()
        game.take_next_turn()
        game.ui.render()
        assert print_history == [
            "Type your next guess ____ you have 5 lives left :",
            "Type your next guess _es_ you have 5 lives left :",
            "You already guessed the letter s",
            "Type your next guess _es_ you have 5 lives left :",
            "Your guess of q is not in the word you lost a life!",
        ]

        game.continue_game()
        game.take_next_turn()
        game.continue_game()
        game.ui.render()

        assert print_history == [
            "Type your next guess ____ you have 5 lives left :",
            "Type your next guess _es_ you have 5 lives left :",
            "You already guessed the letter s",
            "Type your next guess _es_ you have 5 lives left :",
            "Your guess of q is not in the word you lost a life!",
            "Type your next guess _es_ you have 4 lives left :",
            "Guessed correctly game finished with 4 lives left",
        ]


def test_stepped_game():
    "More data oriented version, turns are taken via data input, this could come from a remote API request for example s"
    game = Game(UiData)
    game.setup({"input": "easy"})
    game.set_word("test")

    game.continue_game()
    assert game.ui.render() == ["Type your next guess ____ you have 5 lives left :"]

    game.take_next_turn({"input": "es"})
    game.continue_game()

    assert game.ui.render() == ["Type your next guess _es_ you have 5 lives left :"]

    game.take_next_turn({"input": "a"})
    game.continue_game()
    assert game.ui.render() == [
        "Your guess of a is not in the word you lost a life!",
        "Type your next guess _es_ you have 4 lives left :",
    ]

    game.take_next_turn({"input": "t"})
    game.continue_game()
    assert game.ui.render() == ["Guessed correctly game finished with 4 lives left"]


def test_select_word():
    assert select_word(0) == None
    assert isinstance(select_word(1), str)
    assert isinstance(select_word(2), str)
    assert isinstance(select_word(3), str)
    assert select_word(4) == None


def test_game_complete():
    game = Game()
    game.set_word("test")
    game.lives = 0
    assert game.continue_game() is False

    game.lives = 6
    assert game.continue_game() is True

    game.hidden_word = ["a", "b"]
    assert game.continue_game() is False


def test_guess_letter():
    game = Game()
    game.set_word("test")

    assert game.hidden_word == ["_", "_", "_", "_"]

    game.guess_letter("t")
    assert game.hidden_word == ["t", "_", "_", "t"]

    game.guess_letter("e")
    assert game.hidden_word == ["t", "e", "_", "t"]
    assert game.lives == 5

    game.guess_letter("f")
    assert game.hidden_word == ["t", "e", "_", "t"]
    assert game.lives == 4

    game.guess_letter("s")
    assert game.hidden_word == ["t", "e", "s", "t"]
    assert game.lives == 4
