# Simple hangman game task

I have implemented the task as a command line application this is partly due to time constraints I would liked to have made something more graphical but in the intrest of completing the task in a timely manner this is what I have gone for.
The solution is written in a way that it could be expanded into a simple web API with a user sending the selected letters and returning the response this is done by injecting a Ui Class to switch how the input is taken and handled.

You can run directly but I have also included a docker file so it can work on any system.

## Native
You may need slight adjustments dependant on platform.
> pip install -r requirements.txt

> python hangman.py

> pytest


## Containerized via Docker

> docker build . -t hangman

### Running the tests

> docker run -it --entrypoint=pytest hangman

### Running the game

> docker run -it hangman
